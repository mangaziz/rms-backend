package com.angaziz.cdc.service;

import com.angaziz.cdc.domain.repository.EmployeeLocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeLocationService {
    private EmployeeLocationRepository employeeLocationRepository;

    @Autowired
    public EmployeeLocationService(EmployeeLocationRepository employeeLocationRepository) {
        this.employeeLocationRepository = employeeLocationRepository;
    }
}

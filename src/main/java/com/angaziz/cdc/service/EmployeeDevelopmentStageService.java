package com.angaziz.cdc.service;

import com.angaziz.cdc.domain.repository.EmployeeDevelopmentStageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeDevelopmentStageService {
    private EmployeeDevelopmentStageRepository employeeDevelopmentStageRepository;

    @Autowired
    public EmployeeDevelopmentStageService(EmployeeDevelopmentStageRepository employeeDevelopmentStageRepository) {
        this.employeeDevelopmentStageRepository = employeeDevelopmentStageRepository;
    }
}

package com.angaziz.cdc.service;

import com.angaziz.cdc.domain.repository.EmployeeEmploymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeEmploymentService {
    private EmployeeEmploymentRepository employeeEmploymentRepository;

    @Autowired
    public EmployeeEmploymentService(EmployeeEmploymentRepository employeeEmploymentRepository) {
        this.employeeEmploymentRepository = employeeEmploymentRepository;
    }
}

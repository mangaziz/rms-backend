package com.angaziz.cdc.service;

import com.angaziz.cdc.domain.repository.EmployeeFamilyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeFamilyService {
    private EmployeeFamilyRepository employeeFamilyRepository;

    @Autowired
    public EmployeeFamilyService(EmployeeFamilyRepository employeeFamilyRepository) {
        this.employeeFamilyRepository = employeeFamilyRepository;
    }
}

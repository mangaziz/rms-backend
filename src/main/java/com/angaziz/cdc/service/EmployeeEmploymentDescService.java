package com.angaziz.cdc.service;

import com.angaziz.cdc.domain.repository.EmployeeEmploymentDescRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeEmploymentDescService {
    private EmployeeEmploymentDescRepository employeeEmploymentDescRepository;

    @Autowired
    public EmployeeEmploymentDescService(EmployeeEmploymentDescRepository employeeEmploymentDescRepository) {
        this.employeeEmploymentDescRepository = employeeEmploymentDescRepository;
    }
}

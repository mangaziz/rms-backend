package com.angaziz.cdc.service;

import com.angaziz.cdc.domain.repository.DevelopmentStageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DevelopmentStageService {
    private DevelopmentStageRepository developmentStageRepository;

    @Autowired
    public DevelopmentStageService(DevelopmentStageRepository developmentStageRepository) {
        this.developmentStageRepository = developmentStageRepository;
    }
}

package com.angaziz.cdc.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    private String httpAccessScope(String ...scopes) {
        StringBuilder scopeAccess = new StringBuilder();
        for(int i = 0; i < scopes.length; i++) {
            scopeAccess.append("#oauth2.hasScope('"+ scopes[i] +"')");

            if(i < (scopes.length - 1)) {
                scopeAccess.append(" or ");
            }
        }

        return String.valueOf(scopeAccess);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                // DEVELOPMENT_STAGE
                .antMatchers(HttpMethod.GET, "/developmentStages/*").access(httpAccessScope("ROOT", "ROOT_DEVELOPMENT_STAGE", "READ_DEVELOPMENT_STAGE"))
                .antMatchers(HttpMethod.GET, "/developmentStages/*/*").access(httpAccessScope("ROOT", "ROOT_DEVELOPMENT_STAGE", "READ_DEVELOPMENT_STAGE_DETAIL"))
                .antMatchers(HttpMethod.GET, "/developmentStages").access(httpAccessScope("ROOT", "ROOT_DEVELOPMENT_STAGE", "READ_DEVELOPMENT_STAGES"))
                .antMatchers(HttpMethod.POST, "/developmentStages").access(httpAccessScope("ROOT", "ROOT_DEVELOPMENT_STAGE", "CREATE_DEVELOPMENT_STAGE"))
                .antMatchers(HttpMethod.PUT, "/developmentStages/*").access(httpAccessScope("ROOT", "ROOT_DEVELOPMENT_STAGE", "UPDATE_DEVELOPMENT_STAGE"))
                .antMatchers(HttpMethod.DELETE, "/developmentStages/*").access(httpAccessScope("ROOT", "ROOT_DEVELOPMENT_STAGE", "DELETE_DEVELOPMENT_STAGE"))
                // DIVISION
                .antMatchers(HttpMethod.GET, "/divisions/*").access(httpAccessScope("ROOT", "ROOT_DIVISION", "READ_DIVISION"))
                .antMatchers(HttpMethod.GET, "/divisions/*/*").access(httpAccessScope("ROOT", "ROOT_DIVISION", "READ_DIVISION_DETAIL"))
                .antMatchers(HttpMethod.GET, "/divisions").access(httpAccessScope("ROOT", "ROOT_DIVISION", "READ_DIVISIONS"))
                .antMatchers(HttpMethod.POST, "/divisions").access(httpAccessScope("ROOT", "ROOT_DIVISION", "CREATE_DIVISION"))
                .antMatchers(HttpMethod.PUT, "/divisions/*").access(httpAccessScope("ROOT", "ROOT_DIVISION", "UPDATE_DIVISION"))
                .antMatchers(HttpMethod.DELETE, "/divisions/*").access(httpAccessScope("ROOT", "ROOT_DIVISION", "DELETE_DIVISION"))
                // EMPLOYEES
                .antMatchers(HttpMethod.GET, "/employees/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE", "READ_EMPLOYEE"))
                .antMatchers(HttpMethod.GET, "/employees/*/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE", "READ_EMPLOYEE_DETAIL"))
                .antMatchers(HttpMethod.GET, "/employees").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE", "READ_EMPLOYEES"))
                .antMatchers(HttpMethod.POST, "/employees").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE", "CREATE_EMPLOYEE"))
                .antMatchers(HttpMethod.PUT, "/employees/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE", "UPDATE_EMPLOYEE"))
                .antMatchers(HttpMethod.DELETE, "/employees/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE", "DELETE_EMPLOYEE"))
                // EMPLOYEE_ADDRESSES
                .antMatchers(HttpMethod.GET, "/employeeAddresses/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_ADDRESS", "READ_EMPLOYEE_ADDRESS"))
                .antMatchers(HttpMethod.GET, "/employeeAddresses/*/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_ADDRESS", "READ_EMPLOYEE_ADDRESS_DETAIL"))
                .antMatchers(HttpMethod.GET, "/employeeAddresses").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_ADDRESS", "READ_EMPLOYEE_ADDRESSES"))
                .antMatchers(HttpMethod.POST, "/employeeAddresses").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_ADDRESS", "CREATE_EMPLOYEE_ADDRESS"))
                .antMatchers(HttpMethod.PUT, "/employeeAddresses/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_ADDRESS", "UPDATE_EMPLOYEE_ADDRESS"))
                .antMatchers(HttpMethod.DELETE, "/employeeAddresses/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_ADDRESS", "DELETE_EMPLOYEE_ADDRESS"))
                // EMPLOYEE_DEVELOPMENT_STAGES
                .antMatchers(HttpMethod.GET, "/employeeDevelopmentStages/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_DEVELOPMENT_STAGE", "READ_EMPLOYEE_DEVELOPMENT_STAGE"))
                .antMatchers(HttpMethod.GET, "/employeeDevelopmentStages/*/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_DEVELOPMENT_STAGE", "READ_EMPLOYEE_DEVELOPMENT_STAGE_DETAIL"))
                .antMatchers(HttpMethod.GET, "/employeeDevelopmentStages").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_DEVELOPMENT_STAGE", "READ_EMPLOYEE_DEVELOPMENT_STAGES"))
                .antMatchers(HttpMethod.POST, "/employeeDevelopmentStages").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_DEVELOPMENT_STAGE", "CREATE_EMPLOYEE_DEVELOPMENT_STAGE"))
                .antMatchers(HttpMethod.PUT, "/employeeDevelopmentStages/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_DEVELOPMENT_STAGE", "UPDATE_EMPLOYEE_DEVELOPMENT_STAGE"))
                .antMatchers(HttpMethod.DELETE, "/employeeDevelopmentStages/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_DEVELOPMENT_STAGE", "DELETE_EMPLOYEE_DEVELOPMENT_STAGE"))
                // EMPLOYEE_EMPLOYMENTS
                .antMatchers(HttpMethod.GET, "/employeeEmployments/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT", "READ_EMPLOYEE_EMPLOYMENT"))
                .antMatchers(HttpMethod.GET, "/employeeEmployments/*/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT", "READ_EMPLOYEE_EMPLOYMENT_DETAIL"))
                .antMatchers(HttpMethod.GET, "/employeeEmployments").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT", "READ_EMPLOYEE_EMPLOYMENTS"))
                .antMatchers(HttpMethod.POST, "/employeeEmployments").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT", "CREATE_EMPLOYEE_EMPLOYMENT"))
                .antMatchers(HttpMethod.PUT, "/employeeEmployments/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT", "UPDATE_EMPLOYEE_EMPLOYMENT"))
                .antMatchers(HttpMethod.DELETE, "/employeeEmployments/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT", "DELETE_EMPLOYEE_EMPLOYMENT"))
                // EMPLOYEE_EMPLOYMENT_DESCS
                .antMatchers(HttpMethod.GET, "/employeeEmploymentDescs/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT_DESC", "READ_EMPLOYEE_EMPLOYMENT_DESC"))
                .antMatchers(HttpMethod.GET, "/employeeEmploymentDescs/*/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT_DESC", "READ_EMPLOYEE_EMPLOYMENT_DESC_DETAIL"))
                .antMatchers(HttpMethod.GET, "/employeeEmploymentDescs").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT_DESC", "READ_EMPLOYEE_EMPLOYMENT_DESCS"))
                .antMatchers(HttpMethod.POST, "/employeeEmploymentDescs").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT_DESC", "CREATE_EMPLOYEE_EMPLOYMENT_DESC"))
                .antMatchers(HttpMethod.PUT, "/employeeEmploymentDescs/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT_DESC", "UPDATE_EMPLOYEE_EMPLOYMENT_DESC"))
                .antMatchers(HttpMethod.DELETE, "/employeeEmploymentDescs/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_EMPLOYMENT_DESC", "DELETE_EMPLOYEE_EMPLOYMENT_DESC"))
                // EMPLOYEE_FAMILIES
                .antMatchers(HttpMethod.GET, "/employeeFamilies/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_FAMILY", "READ_EMPLOYEE_FAMILY"))
                .antMatchers(HttpMethod.GET, "/employeeFamilies/*/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_FAMILY", "READ_EMPLOYEE_FAMILY_DETAIL"))
                .antMatchers(HttpMethod.GET, "/employeeFamilies").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_FAMILY", "READ_EMPLOYEE_FAMILIES"))
                .antMatchers(HttpMethod.POST, "/employeeFamilies").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_FAMILY", "CREATE_EMPLOYEE_FAMILY"))
                .antMatchers(HttpMethod.PUT, "/employeeFamilies/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_FAMILY", "UPDATE_EMPLOYEE_FAMILY"))
                .antMatchers(HttpMethod.DELETE, "/employeeFamilies/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_FAMILY", "DELETE_EMPLOYEE_FAMILY"))
                // EMPLOYEE_LOCATIONS
                .antMatchers(HttpMethod.GET, "/employeeLocations/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_LOCATION", "READ_EMPLOYEE_LOCATION"))
                .antMatchers(HttpMethod.GET, "/employeeLocations/*/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_LOCATION", "READ_EMPLOYEE_LOCATION_DETAIL"))
                .antMatchers(HttpMethod.GET, "/employeeLocations").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_LOCATION", "READ_EMPLOYEE_LOCATIONS"))
                .antMatchers(HttpMethod.POST, "/employeeLocations").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_LOCATION", "CREATE_EMPLOYEE_LOCATION"))
                .antMatchers(HttpMethod.PUT, "/employeeLocations/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_LOCATION", "UPDATE_EMPLOYEE_LOCATION"))
                .antMatchers(HttpMethod.DELETE, "/employeeLocations/*").access(httpAccessScope("ROOT", "ROOT_EMPLOYEE_LOCATION", "DELETE_EMPLOYEE_LOCATION"))
                // LOCATIONS
                .antMatchers(HttpMethod.GET, "/locations/*").access(httpAccessScope("ROOT", "ROOT_LOCATION", "READ_LOCATION"))
                .antMatchers(HttpMethod.GET, "/locations/*/*").access(httpAccessScope("ROOT", "ROOT_LOCATION", "READ_LOCATION_DETAIL"))
                .antMatchers(HttpMethod.GET, "/locations").access(httpAccessScope("ROOT", "ROOT_LOCATION", "READ_LOCATIONS"))
                .antMatchers(HttpMethod.POST, "/locations").access(httpAccessScope("ROOT", "ROOT_LOCATION", "CREATE_LOCATION"))
                .antMatchers(HttpMethod.PUT, "/locations/*").access(httpAccessScope("ROOT", "ROOT_LOCATION", "UPDATE_LOCATION"))
                .antMatchers(HttpMethod.DELETE, "/locations/*").access(httpAccessScope("ROOT", "ROOT_LOCATION", "DELETE_LOCATION"));
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId("rms-resource");
    }
}

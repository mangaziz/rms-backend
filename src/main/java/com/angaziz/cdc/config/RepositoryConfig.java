package com.angaziz.cdc.config;

import com.angaziz.cdc.domain.entity.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Employee.class)
                .exposeIdsFor(EmployeeDevelopmentStage.class)
                .exposeIdsFor(DevelopmentStage.class)
                .exposeIdsFor(Location.class)
                .exposeIdsFor(Division.class);
    }
}

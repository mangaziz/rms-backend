package com.angaziz.cdc;

import org.flywaydb.core.Flyway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
@EnableResourceServer
public class RmsApplication {
	public static void main(String[] args) {
	    // Start migration before execution
        Flyway flyway = new Flyway();
        flyway.setDataSource("jdbc:mysql://localhost:3306/learn_rms", "root", "toordev");
        flyway.migrate();

		SpringApplication.run(RmsApplication.class, args);
	}
}

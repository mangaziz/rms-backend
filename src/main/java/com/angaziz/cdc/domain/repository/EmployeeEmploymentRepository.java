package com.angaziz.cdc.domain.repository;

import com.angaziz.cdc.domain.entity.EmployeeEmployment;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeEmploymentRepository extends PagingAndSortingRepository<EmployeeEmployment, Integer> {
}

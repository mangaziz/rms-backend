package com.angaziz.cdc.domain.repository;

import com.angaziz.cdc.domain.entity.EmployeeAddress;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeAddressRepository extends PagingAndSortingRepository<EmployeeAddress, Integer> {
}

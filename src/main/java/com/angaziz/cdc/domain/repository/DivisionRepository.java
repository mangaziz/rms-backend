package com.angaziz.cdc.domain.repository;

import com.angaziz.cdc.domain.entity.Division;
import org.springframework.data.repository.CrudRepository;

public interface DivisionRepository extends CrudRepository<Division, Integer> {
}

package com.angaziz.cdc.domain.repository;

import com.angaziz.cdc.domain.entity.Location;
import org.springframework.data.repository.CrudRepository;

public interface LocationRepository extends CrudRepository<Location, Integer> {
}

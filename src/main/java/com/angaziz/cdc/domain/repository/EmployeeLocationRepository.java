package com.angaziz.cdc.domain.repository;

import com.angaziz.cdc.domain.entity.EmployeeLocation;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeLocationRepository extends PagingAndSortingRepository<EmployeeLocation, Integer> {
}

package com.angaziz.cdc.domain.repository;

import com.angaziz.cdc.domain.entity.EmployeeFamily;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeFamilyRepository extends PagingAndSortingRepository<EmployeeFamily, Integer> {
}

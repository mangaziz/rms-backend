package com.angaziz.cdc.domain.repository;

import com.angaziz.cdc.domain.entity.Employee;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Integer> {
    Employee findOneById(int Id);
    Employee findOneByUsername(String username);
}

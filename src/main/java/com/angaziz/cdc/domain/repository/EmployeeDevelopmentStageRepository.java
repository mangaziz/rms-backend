package com.angaziz.cdc.domain.repository;

import com.angaziz.cdc.domain.entity.EmployeeDevelopmentStage;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeDevelopmentStageRepository extends PagingAndSortingRepository<EmployeeDevelopmentStage, Integer> {
}

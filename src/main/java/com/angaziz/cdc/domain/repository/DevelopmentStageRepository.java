package com.angaziz.cdc.domain.repository;

import com.angaziz.cdc.domain.entity.DevelopmentStage;
import org.springframework.data.repository.CrudRepository;

public interface DevelopmentStageRepository extends CrudRepository<DevelopmentStage, Integer> {
}

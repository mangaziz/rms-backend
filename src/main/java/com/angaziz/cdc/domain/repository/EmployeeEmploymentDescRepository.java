package com.angaziz.cdc.domain.repository;

import com.angaziz.cdc.domain.entity.EmployeeEmploymentDesc;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeEmploymentDescRepository extends PagingAndSortingRepository<EmployeeEmploymentDesc, Integer> {
}

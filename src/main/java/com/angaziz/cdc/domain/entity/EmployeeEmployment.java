package com.angaziz.cdc.domain.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "employee_employments")
public class EmployeeEmployment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "start_date")
    private Date startDate;
    @Column(name = "end_date")
    private Date endDate;
    @Size(max = 255)
    private String company;
    @Size(max = 255)
    private String position;

    // Relationships
    @ManyToOne(targetEntity = Employee.class)
    @JoinColumn(name = "employee_id")
    private Employee employee;
    @OneToMany(mappedBy = "employment")
    @OrderBy(value = "id ASC")
    private Set<EmployeeEmploymentDesc> employeeEmploymentDescs = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Set<EmployeeEmploymentDesc> getEmployeeEmploymentDescs() {
        return employeeEmploymentDescs;
    }

    public void setEmployeeEmploymentDescs(Set<EmployeeEmploymentDesc> employeeEmploymentDescs) {
        this.employeeEmploymentDescs = employeeEmploymentDescs;
    }
}

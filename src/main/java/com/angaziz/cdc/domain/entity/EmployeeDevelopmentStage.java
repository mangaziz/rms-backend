package com.angaziz.cdc.domain.entity;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "employee_development_stages")
public class EmployeeDevelopmentStage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "start_date")
    private Date startDate;
    @Column(name = "end_date")
    private Date endDate;

    // Relationships
    @ManyToOne(targetEntity = Employee.class)
    @JoinColumn(name = "employee_id")
    private Employee employee;
    @ManyToOne(targetEntity = DevelopmentStage.class)
    @JoinColumn(name = "development_stage_id")
    private DevelopmentStage developmentStage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public DevelopmentStage getDevelopmentStage() {
        return developmentStage;
    }

    public void setDevelopmentStage(DevelopmentStage developmentStage) {
        this.developmentStage = developmentStage;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}

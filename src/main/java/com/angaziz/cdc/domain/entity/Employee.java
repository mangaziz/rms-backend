package com.angaziz.cdc.domain.entity;

import com.angaziz.cdc.domain.entity.enums.employee.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;

@Entity(name = "employees")
public class Employee implements UserDetails {
    @Id
    @GeneratedValue
    private int id;
    @Pattern(regexp = "(\\w|\\.|_)+@(\\w|.|-)+")
    @NotNull
    private String email;
    @NotNull
    @Size(min = 4, max = 30)
    @Pattern(regexp = "[a-zA-Z]+")
    private String username;
    @JsonIgnore
    @NotNull
    @Size(min = 32, max = 32)
    private String password;
    @Column(name = "first_name")
    @Pattern(regexp = "\\w+")
    @Size(max = 255)
    @NotNull
    private String firstName;
    @Column(name = "last_name")
    @Pattern(regexp = "\\w+")
    @Size(max = 255)
    @NotNull
    private String lastName;
    @Column(name = "birth_date")
//    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd")
    private Date birthDate;
    @Size(max = 50)
    private String nationality;
    @Size(max = 15)
    private String phone;
    @Column(name = "sub_division")
    private String subDivision;
    @NotNull
    private boolean status = true;
    @Column(name = "hired_date")
//    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd")
    private Date hiredDate;
    private String avatar;

    // Enums
    @NotNull
    private Role role = Role.USER;
    private Gender gender;
    @Column(name = "marital_status")
    private MaritalStatus maritalStatus;

    // Relationships
    @OneToOne(targetEntity = Division.class)
    @JoinColumn(name = "division_id")
    private Division division;
    @OneToMany(mappedBy = "employee")
    @OrderBy(value = "id DESC")
    private Set<EmployeeDevelopmentStage> employeeDevelopmentStages = new HashSet<>();
    @OneToMany(mappedBy = "employee")
    @OrderBy(value = "id DESC")
    private Set<EmployeeEmployment> employeeEmployments = new HashSet<>();
    @OneToMany(mappedBy = "employee")
    @OrderBy(value = "id ASC")
    private Set<EmployeeFamily> employeeFamilies = new HashSet<>();
    @OneToMany(mappedBy = "employee")
    @OrderBy(value = "id DESC")
    private Set<EmployeeLocation> employeeLocations = new HashSet<>();
    @OneToMany(mappedBy = "employee")
    @OrderBy(value = "id DESC")
    private Set<EmployeeAddress> employeeAddresses = new HashSet<>();

    // Timestamps
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        return authorities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSubDivision() {
        return subDivision;
    }

    public void setSubDivision(String subDivision) {
        this.subDivision = subDivision;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getHiredDate() {
        return hiredDate;
    }

    public void setHiredDate(Date hiredDate) {
        this.hiredDate = hiredDate;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Division getDivision() {
        return division;
    }

    public void setDivision(Division division) {
        this.division = division;
    }

    public Set<EmployeeDevelopmentStage> getEmployeeDevelopmentStages() {
        return employeeDevelopmentStages;
    }

    public void setEmployeeDevelopmentStages(Set<EmployeeDevelopmentStage> employeeDevelopmentStages) {
        this.employeeDevelopmentStages = employeeDevelopmentStages;
    }

    public Set<EmployeeEmployment> getEmployeeEmployments() {
        return employeeEmployments;
    }

    public void setEmployeeEmployments(Set<EmployeeEmployment> employeeEmployments) {
        this.employeeEmployments = employeeEmployments;
    }

    public Set<EmployeeFamily> getEmployeeFamilies() {
        return employeeFamilies;
    }

    public void setEmployeeFamilies(Set<EmployeeFamily> employeeFamilies) {
        this.employeeFamilies = employeeFamilies;
    }

    public Set<EmployeeLocation> getEmployeeLocations() {
        return employeeLocations;
    }

    public void setEmployeeLocations(Set<EmployeeLocation> employeeLocations) {
        this.employeeLocations = employeeLocations;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @PrePersist
    protected void onCreate() {
        createdAt = new Date();
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    @PreUpdate
    protected void onUpdate() {
        updatedAt = new Date();
    }
}

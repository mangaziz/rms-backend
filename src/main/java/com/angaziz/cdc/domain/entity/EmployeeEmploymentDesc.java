package com.angaziz.cdc.domain.entity;

import javax.persistence.*;

@Entity(name = "employee_employment_descriptions")
public class EmployeeEmploymentDesc {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String description;

    // Relationships
    @ManyToOne(targetEntity = EmployeeEmployment.class)
    @JoinColumn(name = "employee_employment_id")
    private EmployeeEmployment employment;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EmployeeEmployment getEmployment() {
        return employment;
    }

    public void setEmployment(EmployeeEmployment employment) {
        this.employment = employment;
    }
}

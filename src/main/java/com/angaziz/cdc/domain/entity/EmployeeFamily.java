package com.angaziz.cdc.domain.entity;

import com.angaziz.cdc.domain.entity.enums.employee.FamilyType;
import com.angaziz.cdc.domain.entity.enums.employee.Gender;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity(name = "employee_families")
public class EmployeeFamily {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Size(max = 255)
    private String name;
    private Date birthDate;
    @Column(name = "is_active")
    private boolean isActive;

    // Enums
    private Gender gender;
    private FamilyType type;

    // Relationship
    @ManyToOne(targetEntity = Employee.class)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public FamilyType getType() {
        return type;
    }

    public void setType(FamilyType type) {
        this.type = type;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}

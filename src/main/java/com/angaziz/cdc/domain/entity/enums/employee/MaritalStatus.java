package com.angaziz.cdc.domain.entity.enums.employee;

public enum MaritalStatus {
    Single,Married,Divorced
}

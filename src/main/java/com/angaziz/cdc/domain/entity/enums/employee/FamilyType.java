package com.angaziz.cdc.domain.entity.enums.employee;

public enum FamilyType {
    Father,Mother,Brother,Sister,Husband,Wife,Son,Daughter
}

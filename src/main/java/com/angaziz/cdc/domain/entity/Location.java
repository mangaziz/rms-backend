package com.angaziz.cdc.domain.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "locations")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String address;

    // Relationships
    @OneToMany(mappedBy = "location")
    @OrderBy(value = "id DESC")
    private Set<EmployeeLocation> employeeLocations = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<EmployeeLocation> getEmployeeLocations() {
        return employeeLocations;
    }

    public void setEmployeeLocations(Set<EmployeeLocation> employeeLocations) {
        this.employeeLocations = employeeLocations;
    }
}

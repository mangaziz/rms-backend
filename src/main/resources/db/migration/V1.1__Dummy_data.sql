INSERT INTO `divisions` (`name`) VALUES ('CDC'), ('SWD Red'), ('SWD Green'), ('SWD Blue');

INSERT INTO `development_stages` (`stage`, `grade`) VALUES
    (1, 'SE-JP'), (2, 'SE-JP'), (3, 'SE-JP'),
    (4, 'SE-PG'), (5, 'SE-PG'), (6, 'SE-PG'), (7, 'SE-PG'),
    (8, 'SE-AP'), (9, 'SE-AP'), (10, 'SE-AP'), (11, 'SE-AP'), (12, 'SE-AP'), (13, 'SE-AP'),
    (14, 'SE-AN'), (15, 'SE-AN'), (16, 'SE-AN'), (17, 'SE-AN'), (18, 'SE-AN'), (19, 'SE-AN'), (20, 'SE-AN'), (21, 'SE-AN'), (22, 'SE-AN');

INSERT INTO `locations` (`name`, `address`) VALUES
    ('Jakarta Office', 'Gedung Wirausaha, 8th Floor, Jl. H.R. Rasuna Said Kav. C 5, Jakarta Selatan 12940, Indonesia'),
    ('Bandung Office', 'Jl. Prof. Surya Sumantri No. 8-D, Bandung, 40164, Indonesia'),
    ('Yogyakarta Office', 'Jl. Sidobali No. 2 Muja Muju, Umbulharjo Yogyakarta 55165, Indonesia'),
    ('Bali Office', 'Jl. By Pass Ngurah Rai, gg. Mina Utama No. 1, Suwung, Denpasar 80223 Bali Indonesia');

/* Password: something */
INSERT INTO `employees` (`email`, `username`, `password`, `role`, `first_name`, `last_name`, `gender`, `birth_date`, `nationality`, `marital_status`, `phone`, `division_id`, `sub_division`, `status`, `hired_date`, `suspend_date`, `avatar`, `created_at`, `updated_at`) VALUES
    ('john.doe@mitrais.com', 'johndoe', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', 0, 'John', 'Doe', 0, '1990-05-05', 'Indonesia', 0, '082118939260', 1, NULL, 1, '2017-01-02', NULL, 'http://www.material-ui.com/images/jsa-128.jpg', '2017-03-16 08:01:41', '2017-03-20 13:48:49'),
    ('windyfederighi@mitrais.com', 'windyfederighi', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', 1, 'Windy', 'Federighi', 0, '1990-07-07', 'Indonesia', 0, '08218978987', 1, NULL, 1, '2016-01-02', NULL, 'http://www.material-ui.com/images/uxceo-128.jpg', '2017-03-16 08:01:41', '2017-03-20 13:49:45'),
    ('timcook@mitrais.com', 'timcook', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', 1, 'Tim', 'Cook', 0, '1960-11-01', 'Indonesia', 0, '082187898789', 1, 'Asterx', 1, '2012-06-07', NULL, 'http://pbs.twimg.com/profile_images/378800000483764274/ebce94fb34c055f3dc238627a576d251_reasonably_small.jpeg', '2017-03-29 07:49:17', NULL),
    ('stevejobs@mitrais.com', 'tevejobs', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', 1, 'Steve', 'Jobs', 0, '1960-11-01', 'Indonesia', 1, '082187985482', 1, 'Asterx', 1, '2012-06-07', NULL, 'https://i0.wp.com/iphone7801.files.wordpress.com/2016/06/steve-jobs.jpg?w=128&h=128&crop=1&ssl=1', '2017-03-29 08:07:21', NULL);

INSERT INTO `employee_development_stages`(`employee_id`, `development_stage_id`, `start_date`, `end_date`) VALUES
    (1, 6, '2016-12-01', '2017-02-28'),
    (2, 1, '2017-01-01', NULL),
    (1, 9, '2017-03-01', NULL),
    (3, 4, '2012-06-07', '2013-01-01'),
    (3, 7, '2013-01-02', '2016-01-01'),
    (3, 9, '2016-01-02', NULL);

INSERT INTO `employee_employments` (`employee_id`, `start_date`, `end_date`, `company`, `position`) VALUES
    (1, '2016-01-01', '2016-06-30', 'Bank BTPN', 'Java Developer'),
    (1, '2016-07-01', '2017-01-11', 'Bank Jabar', 'Java Developer'),
    (1, '2017-01-12', NULL, 'Bank CIMB Niaga', 'Java Developer'),
    (2, '2016-04-01', '2017-03-01', 'Telkomsel', 'PHP Developer');

INSERT INTO `employee_employment_descriptions` (`employee_employment_id`, `description`) VALUES
    (1, 'Optimizing performance at customer projects'),
    (1, 'Managing & deploying & releasing production'),
    (1, 'Lead IMB ODC team (7 members)'),
    (1, 'Tools & Technology: InteliJ IDE, Jenkins, Git, Maven, Spring'),
    (2, 'Optimize performance at customer projects'),
    (2, 'Managing & deploying & releasing production'),
    (2, 'Lead IMB ODC team (7 members)'),
    (2, 'Tools & Technology: InteliJ IDE, Jenkins, Git, Maven, Spring'),
    (3, 'Optimize performance at customer projects'),
    (3, 'Managing & deploying & releasing production');

INSERT INTO `employee_families` (`employee_id`, `name`, `gender`, `birth_date`, `type`, `is_active`) VALUES
    (1, 'Michael Doe', 0, '1970-01-10', 0, 1),
    (1, 'Michaela Doe', 1, '1972-03-11', 1, 1),
    (1, 'Jonathan Doe', 0, '1980-04-23', 2, 1),
    (1, 'Doe Doel', 1, '1990-05-24', 2, 1),
    (2, 'Craig Federighi', 0, '1970-01-01', 0, 1),
    (2, 'Marissa Federighi', 1, '1970-01-01', 1, 1);

INSERT INTO `employee_locations` (`employee_id`, `location_id`, `start_date`, `end_date`) VALUES
    (1, 1, '2016-01-01', '2017-01-01'),
    (1, 2, '2017-01-02', null),
    (2, 3, '2012-01-01', null);

INSERT INTO `employee_addresses` (`employee_id`, `start_date`, `end_date`, `address`) VALUES
    (1, '2016-01-01', null, 'Jl. Pinggir Jalan Nomor Sekian, Kab. Kota');
INSERT INTO `oauth_client_details` (`client_id`, `resource_ids`, `client_secret`, `scope`, `authorized_grant_types`, `access_token_validity`) values
    ('client1', 'rms-resource', 'secret', 'READ_EMPLOYEES', 'password', 3600),
    ('client2', 'rms-resource', 'secret', 'READ_EMPLOYEE', 'password', 3600),
    ('client3', 'rms-resource', 'secret', 'CREATE_EMPLOYEE', 'password', 3600),
    ('client4', 'rms-resource', 'secret', 'READ_EMPLOYEE,READ_EMPLOYEES,CREATE_EMPLOYEE', 'password', 3600);
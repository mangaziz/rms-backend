SET FOREIGN_KEY_CHECKS=0
;

DROP TABLE IF EXISTS `development_stages` CASCADE
;

DROP TABLE IF EXISTS `divisions` CASCADE
;

DROP TABLE IF EXISTS `employee_addresses` CASCADE
;

DROP TABLE IF EXISTS `employee_development_stages` CASCADE
;

DROP TABLE IF EXISTS `employee_employment_descriptions` CASCADE
;

DROP TABLE IF EXISTS `employee_employments` CASCADE
;

DROP TABLE IF EXISTS `employee_families` CASCADE
;

DROP TABLE IF EXISTS `employee_locations` CASCADE
;

DROP TABLE IF EXISTS `employees` CASCADE
;

DROP TABLE IF EXISTS `locations` CASCADE
;

CREATE TABLE `development_stages`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`stage` TINYINT NOT NULL,
	`grade` VARCHAR(20) NOT NULL,
	CONSTRAINT `PK_development_stage` PRIMARY KEY (`id` ASC)
)
;

CREATE TABLE `divisions`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	CONSTRAINT `PK_division` PRIMARY KEY (`id` ASC)
)
;

CREATE TABLE `employee_addresses`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`employee_id` INT NOT NULL,
	`start_date` DATE NOT NULL,
	`end_date` DATE 	 NULL,
	`address` TEXT NOT NULL,
	CONSTRAINT `PK_employee_addresses` PRIMARY KEY (`id` ASC)
)
;

CREATE TABLE `employee_development_stages`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`employee_id` INT NOT NULL,
	`development_stage_id` INT NOT NULL,
	`start_date` DATE 	 NULL,
	`end_date` DATE 	 NULL,
	CONSTRAINT `PK_employee_ds` PRIMARY KEY (`id` ASC)
)
;

CREATE TABLE `employee_employment_descriptions`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`employee_employment_id` INT NOT NULL,
	`description` TEXT NOT NULL,
	CONSTRAINT `PK_employee_employment_desc` PRIMARY KEY (`id` ASC)
)
;

CREATE TABLE `employee_employments`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`employee_id` INT NOT NULL,
	`start_date` DATE NOT NULL,
	`end_date` DATE 	 NULL,
	`company` VARCHAR(255) 	 NULL,
	`position` VARCHAR(100) 	 NULL,
	CONSTRAINT `PK_employee_employment` PRIMARY KEY (`id` ASC)
)
;

CREATE TABLE `employee_families`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`employee_id` INT NOT NULL,
	`name` VARCHAR(50) NOT NULL,
	`gender` TINYINT NOT NULL,
	`birth_date` DATE NOT NULL,
	`type` TINYINT NOT NULL,
	`is_active` BOOL NOT NULL,
	CONSTRAINT `PK_employee_family` PRIMARY KEY (`id` ASC)
)
;

CREATE TABLE `employee_locations`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`employee_id` INT NOT NULL,
	`location_id` INT NOT NULL,
	`start_date` DATE NOT NULL,
	`end_date` DATE 	 NULL,
	CONSTRAINT `PK_employee_location` PRIMARY KEY (`id` ASC)
)
;

CREATE TABLE `employees`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(50) NOT NULL,
	`username` VARCHAR(30) NOT NULL,
	`password` VARCHAR(60) NOT NULL,
	`role` TINYINT NOT NULL,
	`first_name` VARCHAR(50) 	 NULL,
	`last_name` VARCHAR(50) 	 NULL,
	`gender` TINYINT 	 NULL,
	`birth_date` DATE 	 NULL,
	`nationality` VARCHAR(50) 	 NULL,
	`marital_status` TINYINT 	 NULL,
	`phone` VARCHAR(15) 	 NULL,
	`division_id` INT 	 NULL,
	`sub_division` VARCHAR(100) 	 NULL,
	`status` BOOL NOT NULL,
	`hired_date` DATE 	 NULL,
	`suspend_date` DATE 	 NULL,
	`avatar` TEXT 	 NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME 	 NULL,
	CONSTRAINT `PK_employee` PRIMARY KEY (`id` ASC)
)
;

CREATE TABLE `locations`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(100) NOT NULL,
	`address` TEXT 	 NULL,
	CONSTRAINT `PK_locations` PRIMARY KEY (`id` ASC)
)
;

ALTER TABLE `employee_addresses` 
 ADD INDEX `IXFK_employee_addresses_employees` (`employee_id` ASC)
;

ALTER TABLE `employee_development_stages` 
 ADD INDEX `IXFK_employee_ds_development_stage` (`development_stage_id` ASC)
;

ALTER TABLE `employee_development_stages` 
 ADD INDEX `IXFK_employee_ds_employee` (`employee_id` ASC)
;

ALTER TABLE `employee_employment_descriptions` 
 ADD INDEX `IXFK_employee_employment_desc_employee_employment` (`employee_employment_id` ASC)
;

ALTER TABLE `employee_employments` 
 ADD INDEX `IXFK_employee_employment_employee` (`employee_id` ASC)
;

ALTER TABLE `employee_families` 
 ADD INDEX `IXFK_employee_family_employee` (`employee_id` ASC)
;

ALTER TABLE `employee_locations` 
 ADD INDEX `IXFK_employee_location_employee` (`employee_id` ASC)
;

ALTER TABLE `employee_locations` 
 ADD INDEX `IXFK_employee_locations_locations` (`location_id` ASC)
;

ALTER TABLE `employees` 
 ADD INDEX `IXFK_employee_division` (`division_id` ASC)
;

ALTER TABLE `employee_addresses` 
 ADD CONSTRAINT `FK_employee_addresses_employees`
	FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `employee_development_stages` 
 ADD CONSTRAINT `FK_employee_ds_development_stage`
	FOREIGN KEY (`development_stage_id`) REFERENCES `development_stages` (`id`) ON DELETE Cascade ON UPDATE Cascade
;

ALTER TABLE `employee_development_stages` 
 ADD CONSTRAINT `FK_employee_ds_employee`
	FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE Cascade ON UPDATE Cascade
;

ALTER TABLE `employee_employment_descriptions` 
 ADD CONSTRAINT `FK_employee_employment_desc_employee_employment`
	FOREIGN KEY (`employee_employment_id`) REFERENCES `employee_employments` (`id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `employee_employments` 
 ADD CONSTRAINT `FK_employee_employment_employee`
	FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE Cascade ON UPDATE Cascade
;

ALTER TABLE `employee_families` 
 ADD CONSTRAINT `FK_employee_family_employee`
	FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE Cascade ON UPDATE Cascade
;

ALTER TABLE `employee_locations` 
 ADD CONSTRAINT `FK_employee_location_employee`
	FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `employee_locations` 
 ADD CONSTRAINT `FK_employee_locations_locations`
	FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `employees` 
 ADD CONSTRAINT `FK_employee_division`
	FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE Cascade ON UPDATE Cascade
;

SET FOREIGN_KEY_CHECKS=1
;


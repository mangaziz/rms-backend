package com.angaziz.cdc;

import com.angaziz.cdc.domain.entity.Employee;
import com.angaziz.cdc.domain.entity.enums.employee.Gender;
import com.angaziz.cdc.domain.entity.enums.employee.MaritalStatus;
import com.angaziz.cdc.domain.entity.enums.employee.Role;
import com.angaziz.cdc.domain.repository.EmployeeRepository;
import io.restassured.response.Response;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.*;
import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.Matchers.*;
import static io.restassured.module.jsv.JsonSchemaValidator.*;
import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiEmployeeTest {

    private String employeesPath = "employees";
    private int expectedGetStatusCode = 200;
    private int expectedPostStatusCode = 201;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void testEmployees() {
        get(employeesPath)
            .then()
            .assertThat()
            .statusCode(expectedGetStatusCode)
            .body(matchesJsonSchemaInClasspath("json-schema/employees.json"));
    }

    @Test
    public void testEmployee() {
        String divisionURL = get(employeesPath+"/1")
            .then()
            .assertThat()
            .statusCode(expectedGetStatusCode)
            .body(matchesJsonSchemaInClasspath("json-schema/employee.json"))
            .extract()
            .path("_links.division.href");

        get(divisionURL)
            .then()
            .assertThat()
            .statusCode(expectedGetStatusCode)
            .body(matchesJsonSchemaInClasspath("json-schema/employeeDivision.json"));
    }

    @Test
    public void testPostEmployee() {
        // Setting up employee data
        DataFactory dataFactory = new DataFactory();
        Employee employee = new Employee();
        employee.setFirstName(dataFactory.getFirstName());
        employee.setLastName(dataFactory.getLastName());
        employee.setEmail(dataFactory.getEmailAddress());
        employee.setUsername(dataFactory.getRandomChars(8, 10));
        employee.setPassword("437b930db84b8079c2dd804a71936b5f");
        employee.setGender(Gender.Male);
        employee.setRole(Role.USER);
        employee.setMaritalStatus(MaritalStatus.Single);

        // Setting up the contentType and body of the request
        // And then retrieve the response of the sent post request
        Response employeeResponse = given()
            .contentType("application/json")
            .body(employee)
        .when()
            .post(employeesPath);


        // Ensure that the expected status code, schema and data is valid
        employeeResponse.then()
            .assertThat()
                .statusCode(expectedPostStatusCode)
                .body(matchesJsonSchemaInClasspath("json-schema/employee.json"))
                .body("firstName", is(employee.getFirstName()))
                .body("lastName", is(employee.getLastName()))
                .body("email", is(employee.getEmail()))
                .body("username", is(employee.getUsername()));

        // Getting the ID from the response
        int employeeId = from(employeeResponse.asString()).getInt("id");

        // Employee data from database
        Employee employeeDataFromDatabase = employeeRepository.findOneById(employeeId);

        // Comparing the response data to actual data at database
        assertThat(employee.getFirstName()).isEqualTo(employeeDataFromDatabase.getFirstName());
        assertThat(employee.getLastName()).isEqualTo(employeeDataFromDatabase.getLastName());
        assertThat(employee.getEmail()).isEqualTo(employeeDataFromDatabase.getEmail());
        assertThat(employee.getUsername()).isEqualTo(employeeDataFromDatabase.getUsername());
    }
}
